package ch.sc.demo.clcache;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClcacheApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClcacheApplication.class, args);
    }

}
