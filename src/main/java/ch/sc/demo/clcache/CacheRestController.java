/*
 * Copyright (c) 2000-2012 Short Consulting AG. All Rights Reserved.
 */
package ch.sc.demo.clcache;

import ch.sc.demo.common.Holiday;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.ehcache.Cache;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.concurrent.ForkJoinPool;
import java.util.function.Supplier;

import static ch.sc.demo.clcache.ClusteredCacheService.HOLIDAYS_CACHE;

/**
 * Author: igor.egorov
 * Date: 31.01.2020
 * Time: 14:24
 */
@RestController
public class CacheRestController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ClusteredCacheService service;

    @GetMapping("/test/helloworld")
    public String hello() {
        return "Hello";
    }

    @GetMapping("/caches/{cacheName}/{key}")
    public String getValue(@PathVariable String cacheName, @PathVariable Long key) {

        if (HOLIDAYS_CACHE.equals(cacheName)) {
            return getValue(cacheName, key, Holiday.class, () -> new Holiday("US", new Date(), "test holiday")).toString();
        } else {
            return getValue(cacheName, key, String.class, () -> UUID.randomUUID().toString());
        }

    }

    @PostMapping("/caches/{cacheName}/{key}")
    public void putValue(@PathVariable String cacheName, @PathVariable Long key, @RequestBody String value) {
        final Cache<Long, String> cache = getClusteredCacheNotNull(cacheName, String.class);
        cache.put(key, value);
    }

    @NotNull
    private <V> V getValue(String cacheName, Long key, Class<V> valueClass, Supplier<V> valueLoader) {
        final Cache<Long, V> cache = getClusteredCacheNotNull(cacheName, valueClass);
        V cached = cache.get(key);
        if (cached == null) {
            V value = valueLoader.get();
            logger.info("Generated:" + value.toString());
            cache.put(key, value);
            return value;
        } else {
            return cached;
        }
    }

    private Cache<Long, ?> getClusteredCacheNotNull(String cacheName) {
        if (HOLIDAYS_CACHE.equals(cacheName)) {
            return getClusteredCacheNotNull(cacheName, Holiday.class);
        } else {
            return getClusteredCacheNotNull(cacheName, String.class);
        }
    }

    private <V> Cache<Long, V> getClusteredCacheNotNull(String cacheName, Class<V> valueClass) {
        final Cache<Long, V> cache = this.service.getClusteredCache(cacheName, valueClass);
        if (cache == null) {
            throw new RuntimeException("Cache is not prepared");
        }
        return cache;
    }

    private <V> Cache<Long, V> getLocalCacheNotNull(String cacheName, Class<V> valueClass) {
        final Cache<Long, V> cache = this.service.getLocalCache(cacheName, valueClass);
        if (cache == null) {
            throw new RuntimeException("Cache is not prepared");
        }
        return cache;
    }

    @DeleteMapping("/caches/{cacheName}/{key}")
    public void deleteValue(@PathVariable String cacheName, @PathVariable Long key) {
        getClusteredCacheNotNull(cacheName).remove(key);
    }

    @DeleteMapping("/caches/{cacheName}")
    public void deleteAllValues(@PathVariable String cacheName) {
        getClusteredCacheNotNull(cacheName).clear();
    }

    @PostMapping("/testPutPerformance")
    public Map<String, Integer> testPutPerformance() throws InterruptedException {
        synchronized (this) {
            logger.info("Test clustered...");
            final Cache<Long, String> clustered = getClusteredCacheNotNull("testPerformanceClustered", String.class);
            doPerformanceTest(clustered);

            logger.info("Test local...");
            final Cache<Long, String> local = getLocalCacheNotNull("testPerformanceLocal", String.class);
            doPerformanceTest(local);

            logger.info("Collect statistics");
            final HashMap<String, Integer> map = new HashMap<>();

            map.put("clustered", countEntries(clustered));
            map.put("local", countEntries(local));
            return map;
        }
    }

    private void doPerformanceTest(Cache<Long, String> cache) throws InterruptedException {
        final MutableBoolean finished = new MutableBoolean(false);
        cache.clear();

         ForkJoinPool.commonPool().submit(() -> {
            long counter = 1L;
            while (!finished.getValue()) {
                cache.put(counter++, "value_" + counter);
            }
        });
        Thread.sleep(5000);
        finished.setTrue();
    }

    private Integer countEntries(Cache<Long, String> cache){
        Integer counter = 0;
        final Iterator<Cache.Entry<Long, String>> iterator = cache.iterator();
        while (iterator.hasNext()){
            iterator.next();
            counter++;
        }
        return counter;
    }
}
