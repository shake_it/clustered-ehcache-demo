/*
 * Copyright (c) 2000-2012 Short Consulting AG. All Rights Reserved.
 */
package ch.sc.demo.clcache;

import org.ehcache.Cache;
import org.ehcache.PersistentCacheManager;
import org.ehcache.clustered.client.config.builders.ClusteredResourcePoolBuilder;
import org.ehcache.clustered.client.config.builders.ClusteredStoreConfigurationBuilder;
import org.ehcache.clustered.client.config.builders.ClusteringServiceConfigurationBuilder;
import org.ehcache.clustered.common.Consistency;
import org.ehcache.config.CacheConfiguration;
import org.ehcache.config.builders.*;
import org.ehcache.config.units.MemoryUnit;
import org.ehcache.event.CacheEventListener;
import org.ehcache.event.EventType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.net.URI;
import java.time.Duration;

/**
 * Author: igor.egorov
 * Date: 31.01.2020
 * Time: 14:29
 */
@Service
public class ClusteredCacheService {

    private Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Value("${app.name}")
    private String appName;

    public static final String HOLIDAYS_CACHE = "holidays";

    public ClusteredCacheService() {
    }

    private PersistentCacheManager cacheManager;

    @PostConstruct
    private void init() {
        LOG.info("Constructing caches for '" + appName + "' ...");
        CacheManagerBuilder<PersistentCacheManager> cacheManagerBuilder = CacheManagerBuilder.newCacheManagerBuilder()
                .with(ClusteringServiceConfigurationBuilder
                        .cluster(URI.create("terracotta://localhost/" + appName))
                        .autoCreate(b -> b.defaultServerResource("primary-server-resource").resourcePool("default-pool", 8, MemoryUnit.MB))
                );

        this.cacheManager = cacheManagerBuilder.build(true);
        
        LOG.info("Caches constructed...");
    }

    public <V> Cache<Long, V> getClusteredCache(String cacheName, Class<V> valueClass){

        if (this.cacheManager != null) {

            final Cache<Long, V> existingCache = this.cacheManager.getCache(cacheName, Long.class, valueClass);
            if(existingCache != null){
                return existingCache;
            }

            final ResourcePoolsBuilder resourcePoolBuilder = ResourcePoolsBuilder
                    .heap(10000000)
                    .with(ClusteredResourcePoolBuilder.clusteredShared("default-pool"));

            CacheEventListenerConfigurationBuilder cacheEventListenerConfiguration = CacheEventListenerConfigurationBuilder
                    .newEventListenerConfiguration(
                            (CacheEventListener<V, V>) event -> LOG.info("Cache event for " + cacheName + ":" + event.getKey().toString() + "->" + event.getNewValue() + " (" + event.getType().name() + ")"),
                            EventType.CREATED, EventType.UPDATED)
                    .unordered().asynchronous();


            CacheConfiguration<Long, V> config = CacheConfigurationBuilder.newCacheConfigurationBuilder(Long.class, valueClass, resourcePoolBuilder)
                    .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(60)))
                    .withService(cacheEventListenerConfiguration)
                    .withService(ClusteredStoreConfigurationBuilder.withConsistency(Consistency.EVENTUAL))
                    .build();

            Cache<Long, V> cache = cacheManager.createCache(cacheName, config);
            LOG.info("Cache created!");
            return cache;
        } else {
            throw new IllegalStateException("Cache manager is not initialized");
        }
    }

    public <V> Cache<Long, V> getLocalCache(String cacheName, Class<V> valueClass){

        if (this.cacheManager != null) {

            final Cache<Long, V> existingCache = this.cacheManager.getCache(cacheName, Long.class, valueClass);
            if(existingCache != null){
                return existingCache;
            }

            final ResourcePoolsBuilder resourcePoolBuilder = ResourcePoolsBuilder
                    .heap(10000000);

            CacheConfiguration<Long, V> config = CacheConfigurationBuilder.newCacheConfigurationBuilder(Long.class, valueClass, resourcePoolBuilder)
                    .build();

            Cache<Long, V> cache = cacheManager.createCache(cacheName, config);
            LOG.info("Cache created!");
            return cache;
        } else {
            throw new IllegalStateException("Cache manager is not initialized");
        }
    }
}
