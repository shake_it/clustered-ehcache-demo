/*
 * Copyright (c) 2000-2012 Short Consulting AG. All Rights Reserved.
 */
package ch.sc.demo.common;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;
import java.util.StringJoiner;

/**
 * Author: igor.egorov
 * Date: 05.02.2020
 * Time: 13:42
 */
@XmlRootElement
public class Holiday implements Serializable {

    @NotNull
    @XmlElement
	protected String calendar;

    @NotNull
    @XmlElement
    protected Date date;

    @Nullable
    @XmlElement
    protected String name;

	public Holiday(@NotNull String calendar, @NotNull Date date, @Nullable String name) {
		this.date = date;
        this.name = name;
        this.calendar = calendar;
	}

	@NotNull
    public String getCalendar() {
		return new String(calendar);
	}

    @NotNull
    public Date getDate() {
        return new Date(date.getTime());
    }

    @Nullable
    public String getName() {
        if (name == null) {
            return null;
        }
        return new String(name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Holiday)) return false;

        Holiday holiday = (Holiday) o;

        if (!calendar.equals(holiday.calendar)) return false;
        if (!date.equals(holiday.date)) return false;
        if (name != null ? !name.equals(holiday.name) : holiday.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = calendar.hashCode();
        result = 31 * result + date.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Holiday.class.getSimpleName() + "[", "]")
                .add("calendar='" + calendar + "'")
                .add("date=" + date.getTime())
                .add("name='" + name + "'")
                .toString();
    }
}
